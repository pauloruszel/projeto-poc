package br.com.mj.poc.dto;

import java.io.Serializable;

public class SistemaDTO implements Serializable {

    private static final long serialVersionUID = -4267164011041609277L;

    private String nome;
    private String url;
    private String descricao;

    public SistemaDTO() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

}
