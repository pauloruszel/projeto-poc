import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";


const routes : Routes = [
  {
    path: 'usuario',
    loadChildren: () => import('./funcionalidades/usuario/usuario.module').then(m => m.UsuarioModule)
  },
  {
    path: 'sistema',
    loadChildren: () => import('./funcionalidades/sistema/sistema.module').then(m => m.SistemaModule)
  }
];


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes, {useHash: true})
  ],
  exports: [ RouterModule ]

})

export class AppRoutingModule { }
